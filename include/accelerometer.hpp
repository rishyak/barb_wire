#include <Adafruit_ADXL345_U.h>
#include <Arduino.h>

namespace Accelerometer
{
    // Connect to the Accelerometer on the hardware I2C port
    Adafruit_ADXL345_Unified accel{Adafruit_ADXL345_Unified()};

    /**
     * @brief initialize accelerometer
     *
     * @note uses serial
     * @warning blocking operation; infinite loop if module not found
     *
     * @authors Rishyak, Aaron
     */
    void initialize()
    {
        if (!accel.begin())
            while (1)
                Serial.println("Error: No ADXL345 accelerometer detected.");
        Serial.println("Initializing accelerometer, please wait...");
        accel.setRange(ADXL345_RANGE_16_G);
        delay(500);
        Serial.println("Accelerometer init!");
    }
} // namespace Accelerometer