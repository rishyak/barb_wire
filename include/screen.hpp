#include <Adafruit_ST7789.h>
namespace Screen
{
    // Connect to the TFT on the hardware I2C port
    Adafruit_ST7789 tft = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);

    /**
     * @brief initialize screen
     *
     * @authors Adafruit, Aaron
     */
    void initialize()
    {
        Serial.print("Initializing TFT display, please wait...");

        // turn on backlite, wait why is it "lite"
        pinMode(TFT_BACKLITE, OUTPUT);
        digitalWrite(TFT_BACKLITE, HIGH);

        // turn on the TFT / I2C power supply
        pinMode(TFT_I2C_POWER, OUTPUT);
        digitalWrite(TFT_I2C_POWER, HIGH);
        delay(10);

        // initialize TFT
        tft.init(135, 240); // Init ST7789 240x135
        tft.setRotation(3);
        tft.fillScreen(ST77XX_BLACK);

        Serial.println("TFT display init!");
    }
    /**
     * @brief asks user to confirm they don't have a seizure
     *
     * @authors Aaron, Vincent
     *
     * @return false user does not have a seizure
     * @return true user has seizure
     */
    bool seizure_check()
    {
        // reset
        tft.fillScreen(ST77XX_BLACK);
        // increase size and set color to red
        tft.setTextSize(3);
        tft.setTextColor(ST77XX_RED);
        // Writes out secondary seizure check title
        tft.setCursor(60, 0);
        tft.print("SEIZURE");
        tft.setCursor(50, 30);
        tft.print("DETECTED");
        // writes out secondary seizure check body
        tft.setTextSize(2);
        tft.setCursor(0, 60);
        tft.print("Press the D1 button on the left if this is WRONG");
        // loop to check for button press in next 10s
        for (int i{0}; i < 100; i++) // 100*0.1s==10s
        {
            if (digitalRead(1) == HIGH)
            {
                Serial.println("Seizure aborted.");
                tft.fillScreen(ST77XX_BLACK);
                tft.setCursor(0, 60);
                tft.setTextSize(2);
                tft.setTextColor(ST77XX_WHITE);
                tft.print("Reporting aborted!");
                return false; // false alarm
            }
            delay(100);
        }
        tft.fillScreen(ST77XX_BLACK);
        tft.setCursor(0, 60);
        tft.setTextSize(2);
        tft.print("Reporting seizure...");
        // write code here to send message about seizure (Aaron: or maybe we should just call the releveant function in main..cpp)
        return true;
    }
    /**
     * @brief clear screen
     * @author Aaron
     */
    void clear()
    {
        tft.fillScreen(ST77XX_BLACK);
    }
    /**
     * @brief regular display when no seizure
     * @note currently shows time or "GPS off"
     *
     * @param hour the current hour 
     * @param minute the current minute
     * @param fix whether or not we have GPS fixed
     */
    void display(uint8_t hour, uint8_t minute, bool fix){
        tft.setTextColor(ST77XX_WHITE);
        tft.setTextSize(3);
        tft.setCursor(20, 60);
        tft.print(hour);
        tft.print(":");
        tft.println(minute);
        if(!fix){
            tft.setTextSize(2);
        }
    }
}