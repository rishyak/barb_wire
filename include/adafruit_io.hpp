#include "config.h" //gitignored as it has seeeecrehhhs
                    //also includes AdafruitIO_WiFi.h
/**
 * @brief stands for Adafruit Io ofc
 * @authors Adafruit, Frangel, Aaron
 */
namespace AI
{
    AdafruitIO_Feed *digital = io.feed("HELP IM HAVING A SEIZURE");

    /**
     * @brief initialize Adafruit IO
     *
     * @note uses serial
     * @warning blocking operation; infinite loop if can't connect
     *
     * @authors Adafruit, Frangel, Aaron
     */
    void initialize()
    {
        Serial.print("Initializing Adafruit IO...");
        // setting up connection w/ io
        io.connect();
        while (io.status() < AIO_CONNECTED)
        {
            Serial.print(".");
            delay(500);
        }

        // we are connected
        Serial.println();
        Serial.println(io.statusText());
    }

    /**
     * @brief Send seizure datas from GPS to Adafruit IO
     * 
     * @param emergency whether or not emergency services should be called
     * @param lat latitude of seizure
     * @param lon longitude of seizure
     * @param ele altitude/elevation of seizure
     * @return true :successful send
     * @return false :failed send
     */
    bool send(bool emergency, float &lat, float &lon, float &ele){
        return digital->save(emergency, lat, lon, ele);
    }
}