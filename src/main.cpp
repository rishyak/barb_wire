// ugly extraction, please finish splitting before using in main
#include "accelerometer.hpp"
#include "faqueue.hpp"
#include "screen.hpp"
#include "gps.hpp"

#define SNOOZE_TIME 500 // 50s snooze time to supress detection for

faqueue logs{faqueue(50)};          // fake queue to store last 5 secs
float sum_x{0}, sum_y{0}, sum_z{0}; // sum of the last (tentatively) 5 seconds' x, y, and z accelerometer values
                                    // for calculating standard deviation
int i{0};                           // timing var for 5 secs

void setup()
{
    Serial.begin();
    Accelerometer::initialize();
    Screen::initialize();
    GPS::initialize();
    AI::initialize();
}
int get_deviation();
/**
 * @author Aaron
 */
void loop()
{
    // get accelerometer readings and sum for last 5 secs
    sensors_event_t event;
    Accelerometer::accel.getEvent(&event);
    sum_x -= logs.front.acceleration.x, sum_y -= logs.front.acceleration.y, sum_z -= logs.front.acceleration.z;
    logs.p_p(event);
    sum_x += event.acceleration.x, sum_y += event.acceleration.y, sum_z += event.acceleration.z;
    // if we got to (repeating) 5 secs mark check the standard deviation for last 5 secs
    if (++i == 50)
    {
        if (get_deviation() >= 120)
        {
            Serial.println("Seizure detected!");
            if (!Screen::seizure_check()) // ask user if seizure or not
                i = -SNOOZE_TIME;         // snooze/suppress seizure detection for next 50s if no seizure
            else
            {
                GPS::send(false);
                Serial.println("Sent GPS data!");
                for (i = 3000; i > 0; i--) //timer for next 5 mins
                {
                    // repeated code to check if seizure's continuing, expires when i goes to five minutes or no longer seizure-ing
                    delay(100);
                    Accelerometer::accel.getEvent(&event);
                    sum_x -= logs.front.acceleration.x, sum_y -= logs.front.acceleration.y, sum_z -= logs.front.acceleration.z;
                    logs.p_p(event);
                    sum_x += event.acceleration.x, sum_y += event.acceleration.y, sum_z += event.acceleration.z;
                    if (i % 50 == 0)
                    {
                        if (get_deviation() < 120)
                        {
                            Serial.println("Seizure stopped.");
                            // i = -SNOOZE_TIME; // prevent repeat detections due to smoothing
                            break;
                        }
                    }
                }
                if (i == 0)
                {
                    GPS::send(true);
                    Serial.println("Sent emergency GPS data!");
                }
                i = -SNOOZE_TIME;
            }
            // logs = faqueue(50); this causes out of memory or something, wanted to disable smoothing but whatever
            delay(5000);
            Screen::clear();
        }
    }
    delay(100);
}
/**
 * @brief gets the mean standard deviation of all 3 axises
 *
 * @author Aaron
 *
 * @return the mean standard deviation of all 3 axises
 */
int get_deviation()
{
    sensors_event_t devs = logs.stan_devs(sum_x, sum_y, sum_z);
    i = 0; // i apologize for this long code to output
    Serial.print("X: ");
    Serial.print(devs.acceleration.x);
    Serial.print(" Y: ");
    Serial.print(devs.acceleration.y);
    Serial.print(" Z: ");
    Serial.print(devs.acceleration.z);
    Serial.print(" Mean: ");
    Serial.print((devs.acceleration.x + devs.acceleration.y + devs.acceleration.z) / 3);
    Serial.println();
    // finally return
    return (devs.acceleration.x + devs.acceleration.y + devs.acceleration.z) / 3;
}